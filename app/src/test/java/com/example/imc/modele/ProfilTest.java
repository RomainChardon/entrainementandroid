package com.example.imc.modele;

import junit.framework.TestCase;

import org.junit.Test;

public class ProfilTest extends TestCase {

    // Création profil
    private Profil profilFemme = new Profil(67, 165, 35, 0);

    // Résultat IMG
    private float img = (float) 32.2;

    // Message
    private String message = "IMG trop élevé";

    @Test
    public void testGetImg() {
        assertEquals(img, profilFemme.getImg(), (float)0.1);
    }

    @Test
    public void testGetMessage() {
        assertEquals(message, profilFemme.getMessage());
    }
}